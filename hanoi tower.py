def hanoitower(count, A,  B , C):
    if count == 1:
        print(A, "-->", C)
    else:
        hanoitower(count - 1, A ,C , B)
        print(A , "-->",C)
        hanoitower(count - 1 ,B , A , C)
hanoitower(4,"A", "B", "C")