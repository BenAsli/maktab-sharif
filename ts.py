import datetime
def Ttime(start_date, end_date, week_day):
    start_year, start_month, start_day = map(int, start_date.split("-")) #in this case "split" start_date vatible to 3 itheme **year** **month** **day**
    end_year, end_month, end_day = map(int, end_date.split("-"))#in this case "split" end _date vatible to 3 itheme **year** **month** **day**
    startd = datetime.date(start_year, start_month,start_day)# in thes case pass 3 itme of start varible to datime metod
    endd = datetime.date(end_year,end_month , end_day)# in thes case pass 3 itme of end varible to datime metod
    while endd > startd:
        startd += datetime.timedelta(days=1)
        if startd.weekday() == int(week_day):
            final_date = f"{startd.year} {startd.strftime('%B')} {startd.day}"
            yield final_date
gen = Ttime("2020-02-18" , "2020-05-02", 3 )
print(next(gen))
print(next(gen))
print(next(gen))